import hoomd 
import openbabel # conda install -c conda-forge openbabel
import numpy as np

def create_gsd(filename, save_gsd=True):
	mol_file = filename
	input_file_type = "{}".format(filename.split('.')[1])
	ob_convert = openbabel.OBConversion()
	ob_convert.SetInAndOutFormats(input_file_type,"gsd") #in, out
	mol = openbabel.OBMol()
	ob_convert.ReadFile(mol,"molecule_files/{}".format(filename));

	all_types = list(set([obatom.GetType() for obatom in openbabel.OBMolAtomIter(mol)]))
	hoomd.context.initialize("")
	box = hoomd.data.boxdim(Lx=75,Ly=75,Lz=75)
	snap = hoomd.data.make_snapshot(N=mol.NumAtoms(), box=box, particle_types=all_types)

	all_bonds = []
	for obatom in openbabel.OBMolAtomIter(mol):
		#print(obatom.GetType(), obatom.GetIdx(), obatom.GetX(),obatom.GetY(), obatom.GetZ())
		index = obatom.GetIdx() - 1
		# Set coordinates
		coords = np.array([obatom.GetX(), obatom.GetY(), obatom.GetZ()])
		snap.particles.position[index] = coords
		# Set typeid
		type_id = all_types.index(obatom.GetType())
		snap.particles.typeid[obatom.GetIdx()-1] = type_id
		for neighbor in openbabel.OBMolAtomIter(mol):
			i_neighbor = neighbor.GetIdx() - 1
			bond = obatom.GetBond(neighbor)
			if bond != None:
				all_bonds.append(np.array([index,i_neighbor]))

	snap.bonds.resize(len(all_bonds))

	for i, bond in enumerate(all_bonds):
		snap.bonds.group[i]=all_bonds[i]
	snap.bonds.types = ['A']
	snap.bonds.typeid[:] = [0 for i in range(len(all_bonds))]

	system = hoomd.init.read_snapshot(snap)
	all = hoomd.group.all();
	if save_gsd == True:
		gsd_file = "{}.gsd".format(filename.split('.')[0]) 
		hoomd.dump.gsd(gsd_file, period=None, group=all, overwrite=True);
	
	return system, snap
