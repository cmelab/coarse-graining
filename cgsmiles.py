import mbuild as mb
import numpy as np


def centroid(compound):

    """
    compound is an mbuild compound.  Accounts for all atoms in the compound, including hydrogen, to find the
    centroid of the compoound
    """

    labels = [i for i in compound.labels if "[" not in i]
    coords = []
    for atom in labels:
        atom_dict = compound.labels[atom]
        atom_type_coords = [atom_dict[n].xyz[0] for n in range(0, len(atom_dict))]

        for n in atom_type_coords:
            coords.append(n)

    print("")
    print("# OF COORDS RETURNED:")
    print(len(coords))

    xlist = [n[0] for n in coords]
    ylist = [n[1] for n in coords]
    zlist = [n[2] for n in coords]
    x_avg = np.mean(xlist)
    y_avg = np.mean(ylist)
    z_avg = np.mean(zlist)
    geo_center = [x_avg, y_avg, z_avg]

    return geo_center


def split_string(string):

    num_index = []  #  Find all index positions with numbers in the string
    for index, element in enumerate(string):
        a = 0
        try:
            int(element)
        except:
            a = 1
        if a == 0:
            num_index.append(index)

    return num_index


compound_dict = {
    "benzene": "c1ccccc1",
    "p3ht": "c1sccc1CCCCCC",
    "p3ht_tail": "CCCCCC",
    "thiophene": "c1sccc1",
    "itic": "CCCCCCc1ccc(cc1)C2(c3ccc(CCCCCC)cc3)c4cc5c6sc7cc(sc7c6C(c8ccc(CCCCCC)cc8)(c9ccc(CCCCCC)cc9)c5cc4c%10sc%11cc(sc%11c2%10)\C=C%12/C(=O)c%13ccccc%13C%12=C(C#N)C#N)\C=C%14/C(=O)c%15ccccc%15C%14=C(C#N)C#N",
    "ptb7": "CCCCC(CC)COC(=O)c1sc2c(scc2c1F)c3sc4c(OCC(CC)CCCC)c5ccsc5c(OCC(CC)CCCC)c4c3",
    "anthracene": " c1ccc2cc3ccccc3cc2c1",
    "naphthalene": "c1ccc2ccccc2c1",
    "test": "c1sccc1cccccc-c1sccc1cccccc",
}


def compounds(name):
    return compound_dict[name]
