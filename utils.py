import re
import numpy as np
import mbuild as mb
#from rdkit import Chem
import pybel


def pybel_to_mbuild(mol):
    """
    Given a pybel mol, return mbuild Compound
    containing atom type and position and bonding
    other info in pybel mol (e.g. bond order, aromaticity) is lost
    """
    comp = mb.Compound()
    for atom in mol.atoms:
        comp.add(mb.Particle(name=atom.type, pos=np.array(atom.coords)/10))
    for i, i_atom in enumerate(mol.atoms[:-1]):
        a = i_atom.OBAtom
        for j, j_atom in enumerate(mol.atoms[i+1:]):
            ind_i = i
            ind_j = j + i + 1
            b = j_atom.OBAtom
            bond = mol.OBMol.GetBond(a,b)
            if bond != None:
                bond_pair = [
                    particle for i, particle in enumerate(comp.particles())
                    if i == ind_i or i == ind_j
                ]
                comp.add_bond(bond_pair)
    return comp


def has_number(string):
    return bool(re.search("[0-9]", string))


def get_bonds(comp):
    """
    Given mbuild compound, return list of bonds
    """
    mol_list = get_mol(comp)
    bond_list = []
    for bond in comp.bonds():
        for i, atom in enumerate(mol_list):
            if (atom["pos"] == bond[0].pos).all():
                atom1 = i
            if (atom["pos"] == bond[1].pos).all():
                atom2 = i
        if atom1 < atom2:
            bond_list.append((atom1, atom2))
        else:
            bond_list.append((atom2, atom1))

    # sort by first then second element of tuple
    bond_list.sort(key=lambda tup: (tup[0], tup[1]))
    return bond_list


def has_common_member(set_a, tup):
    """
    return True if set_a (set) and tup (tuple) share a common member
    else return False
    """
    set_b = set(tup)
    return set_a & set_b


def cg_comp(comp, bead_inds):
    """
    given an mbuild compound and bead_inds(list of tup)
    return coarse-grained mbuild compound
    """
    mol = get_mol(comp)
    N = comp.n_particles

    for bead, smarts, bead_name in bead_inds:
        bead_xyz = np.empty([len(bead), 3])
        for i, atom_ind in enumerate(bead):
            bead_xyz[i, :] = mol[atom_ind]["pos"]
        avg_xyz = np.mean(bead_xyz, axis=0)
        bead = mb.Particle(name=bead_name, pos=avg_xyz)
        bead.smarts_string = smarts
        comp.add(bead)

    return comp, N


def cg_bonds(comp, beads, N):
    """
    add bonds based on bonding in aa compound
    return bonded mbuild compound
    """
    bonds = get_bonds(comp)
    bead_bonds = []
    for i, (bead_i, _, _) in enumerate(beads[:-1]):
        for j, (bead_j, _, _) in enumerate(beads[(i + 1) :]):
            for pair in bonds:
                if (pair[0] in bead_i) and (pair[1] in bead_j):
                    bead_bonds.append((i, j + i + 1))
                if (pair[1] in bead_i) and (pair[0] in bead_j):
                    bead_bonds.append((i, j + i + 1))
    for pair in bead_bonds:
        bond_pair = [
            particle
            for i, particle in enumerate(comp.particles())
            if i == pair[0] + N or i == pair[1] + N
        ]
        comp.add_bond(bond_pair)
    return comp


def remove_atomistic(comp):
    """
    all coarse-grained particles are named starting with '_'
    remove any particles whose names do not start with '_'
    """
    for i in comp.particles():
        if i.name[0] != "_":
            comp.remove(i)
    return comp


def num2str(num):
    """
    Returns a capital letter for positive integers up to 701
    e.g. num2str(0) = 'A'
    """
    if num < 26:
        return chr(num+65)
    return "".join([chr(num//26+64),chr(num%26+65)])


def coarse(mol, bead_smarts, atomistic=False):
    """
    given starting compound and smart strings for desired beads
    - mol(pybel molecule)
    - bead_smarts(list of str)
    return coarse-grain mbuild compound
    atomistic (default=False) will return the coarse-grain compound overlaid
    with the atomistic one
    """
    matches = []
    for i, smart_str in enumerate(bead_smarts):
        smarts = pybel.Smarts(smart_str)
        if not smarts.findall(mol):
            print(f"{smart_str} not found in compound!")
        for group in smarts.findall(mol):
            group = tuple(i - 1 for i in group)
            matches.append((group, smart_str, f"_{num2str(i)}"))

    seen = set()
    bead_inds = []
    for group, smarts, name in matches:
        # smart strings for rings can share atoms
        # add bead regardless of whether it was seen
        if has_number(smarts):
            for atom in group:
                seen.add(atom)
            bead_inds.append((group, smarts, name))
        # alkyl chains should be exclusive
        else:
            if has_common_member(seen, group):
                pass
            else:
                for atom in group:
                    seen.add(atom)
                bead_inds.append((group, smarts, name))

    n_atoms = mol.OBMol.NumHvyAtoms()
    if n_atoms != len(seen):
        print(
            "WARNING: Some atoms have been left out of coarse-graining!"
        )  # TODO make this more informative

    comp = pybel_to_mbuild(mol)
    comp, N = cg_comp(comp, bead_inds)
    comp = cg_bonds(comp, bead_inds, N)

    if not atomistic:
        comp = remove_atomistic(comp)

    return comp


def remove_hydrogens(comp):
    """
    remove all hydrogen atoms in an mbuild compound
    """
    h_atoms = [i for i in comp.particles() if i.name == "H"]
    comp.remove(h_atoms)


def get_mol(comp):
    """
   Given mbuild compound return list of dictionaries containing atom type and position
   """
    mol_list = []
    for atom in comp.particles():
        atom_dict = {"type": atom.name, "pos": atom.pos}
        mol_list.append(atom_dict)
    return mol_list


# I think these aren't used anymore
############################################################################################
# def add_bond(bondpair):
#    """
#    Given the indices of the atoms in mol_list, create bond between those atoms
#    """
#    pair = [
#        atom
#        for i, atom in enumerate(comp.particles())
#        if i == bondpair[0] or i == bondpair[1]
#    ]
#    comp.add_bond(pair)
#
#
# def remove_bond(comp, removed, init_bonds=None, init_mol=None, bond_no=None):
#    """
#    removes a bond in an mbuild compound
#    comp - mbuild compound
#    removed - list of bond pairs (tuple)
#    returns removed
#    """
#    # if no initial bonds are provided, use current
#    if init_bonds == None:
#        init_bonds = get_bonds(comp)
#    if init_mol == None:
#        init_mol = get_mol(comp)
#    # pick random bond if none is provided
#    if bond_no == None:
#        bond_no = random.randint(0, len(init_bonds) - 1)
#
#    bond_pair = init_bonds[bond_no]
#    removed.append(bond_pair)
#    pair = [
#        atom
#        for i, atom in enumerate(comp.particles())
#        if i == bond_pair[0] or i == bond_pair[1]
#    ]
#    comp.remove_bond(pair)
#    print(
#        "Removed bond between {}{} and {}{}".format(
#            init_mol[bond_pair[0]]["type"],
#            bond_pair[0],
#            init_mol[bond_pair[1]]["type"],
#            bond_pair[1],
#        )
#    )
#    return removed
#
#
# def get_all_connected_groups(graph):
#    """
#    returns list of lists of atom indices
#    graph - dictionary of connectivity (must show two-way connectivity,
#    e.g. 1 connected to 2 AND 2 connected to 1)
#    """
#    already_seen = set()
#    result = []
#    for node in graph:
#        if node not in already_seen:
#            connected_group, already_seen = get_connected_group(graph, node, already_seen)
#            result.append(connected_group)
#    return result
#
#
# def get_connected_group(graph, node, already_seen):
#    """
#    For each node in graph determine connected components
#    """
#    result = []
#    nodes = set([node])
#    while nodes:
#        node = nodes.pop()
#        already_seen.add(node)
#        nodes.update(graph[node] - already_seen)
#        result.append(node)
#    return result, already_seen
